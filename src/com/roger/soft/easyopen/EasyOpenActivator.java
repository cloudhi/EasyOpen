package com.roger.soft.easyopen;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The activator class controls the plug-in life cycle
 */
public class EasyOpenActivator extends AbstractUIPlugin {

    // The plug-in ID
    public static final String       PLUGIN_ID         = "com.roger.soft.easyopen";                  //$NON-NLS-1$
    private static final String      TARGETPREFERENCES = "com.roger.soft.easyopen.targetPreferences";

    // The shared instance
    private static EasyOpenActivator plugin;

    /**
     * The constructor
     */
    public EasyOpenActivator() {
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.
     * BundleContext)
     */
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }

    /*
     * (non-Javadoc)
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.
     * BundleContext)
     */
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns the shared instance
     *
     * @return the shared instance
     */
    public static EasyOpenActivator getDefault() {
        return plugin;
    }

    /**
     * Returns an image descriptor for the image file at the given plug-in
     * relative path
     *
     * @param path
     *            the path
     * @return the image descriptor
     */
    public static ImageDescriptor getImageDescriptor(String path) {
        return imageDescriptorFromPlugin(PLUGIN_ID, path);
    }

    @Override
    protected void initializeDefaultPreferences(IPreferenceStore store) {
        String defaultTarget = "explorer.exe %s";
        String osName = System.getProperty("os.name").toUpperCase();
        if (osName.indexOf("WINDOWS") != -1) {
            defaultTarget = "explorer.exe %s";
        } else if (osName.indexOf("MAC") != -1) {
            defaultTarget = "open %s";
        } else if (osName.indexOf("LINUX") != -1) {
            defaultTarget = "nautilus %s";
        }

        store.setDefault(TARGETPREFERENCES, defaultTarget);
    }

    public String getTargetPreferences() {
        return getPreferenceStore().getString(TARGETPREFERENCES);
    }

    public static void log(String message) {
        ILog log = getDefault().getLog();
        Status status = new Status(1, PLUGIN_ID, message);
        log.log(status);
    }

    public static void log(String message, Throwable ex) {
        ILog log = getDefault().getLog();
        Status status = new Status(4, PLUGIN_ID, 4, message, ex);
        log.log(status);
    }

    public boolean isSupported() {
        String osName = System.getProperty("os.name").toUpperCase();
        return ((osName.indexOf("WINDOWS") != -1) || (osName.indexOf("MAC") != -1)) || (osName.indexOf("LINUX") != -1);
    }

}
