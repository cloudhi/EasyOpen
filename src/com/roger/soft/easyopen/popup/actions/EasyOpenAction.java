package com.roger.soft.easyopen.popup.actions;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.jdt.internal.core.JarPackageFragmentRoot;
import org.eclipse.jdt.internal.core.PackageFragment;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import com.roger.soft.easyopen.EasyOpenActivator;

public class EasyOpenAction implements IObjectActionDelegate {

    private Shell               shell;
    private Object              selected;
    private Class               selectedClass = null;
    private static final String title         = "Easy Open";

    /**
     * Constructor for Action1.
     */
    public EasyOpenAction() {
        super();
    }

    /**
     * @see IObjectActionDelegate#setActivePart(IAction, IWorkbenchPart)
     */
    public void setActivePart(IAction action, IWorkbenchPart targetPart) {
        shell = targetPart.getSite().getShell();
    }

    /**
     * @see IActionDelegate#run(IAction)
     */
    public void run(IAction action) {

        try {

            if ("unknown".equals(this.selected)) {
                MessageDialog.openInformation(this.shell, title, "Unable to explore " + this.selectedClass.getName());
                EasyOpenActivator.log("Unable to explore " + this.selectedClass);
                return;
            }
            File directory = null;
            if (this.selected instanceof IResource)
                directory = new File(((IResource) this.selected).getLocation().toOSString());
            else if (this.selected instanceof File) {
                directory = (File) this.selected;
            }
            if (this.selected instanceof IFile) {
                directory = directory.getParentFile();
            }
            if (this.selected instanceof File) {
                directory = directory.getParentFile();
            }
            String target = EasyOpenActivator.getDefault().getTargetPreferences();

            if (!(EasyOpenActivator.getDefault().isSupported())) {
                MessageDialog.openInformation(this.shell, title,
                        "This platform (" + System.getProperty("os.name") + ") is currently unsupported.");
                return;
            }

            if (directory == null) {
                MessageDialog.openInformation(shell, title, "unable to open select file floder");
                return;
            }

            String command = String.format(target, directory.toString());
            try {
                Runtime.getRuntime().exec(command);
            } catch (Throwable t) {
                MessageDialog.openInformation(new Shell(), title, "Unable to execute " + target);
                EasyOpenActivator.log("open file floder exception:" + command, t);
            }
        } catch (Throwable e) {
            EasyOpenActivator.log("open file folder exception:", e);
        }
    }

    /**
     * @see IActionDelegate#selectionChanged(IAction, ISelection)
     */
    public void selectionChanged(IAction action, ISelection selection) {
        try {
            IAdaptable adaptable = null;
            this.selected = "unknown";
            if (selection instanceof IStructuredSelection) {
                adaptable = (IAdaptable) ((IStructuredSelection) selection).getFirstElement();
                this.selectedClass = adaptable.getClass();
                if (adaptable instanceof IResource)
                    this.selected = ((IResource) adaptable);
                else if ((adaptable instanceof PackageFragment)
                        && (((PackageFragment) adaptable).getPackageFragmentRoot() instanceof JarPackageFragmentRoot))
                    this.selected = getJarFile(((PackageFragment) adaptable).getPackageFragmentRoot());
                else if (adaptable instanceof JarPackageFragmentRoot)
                    this.selected = getJarFile(adaptable);
                else
                    this.selected = ((IResource) adaptable.getAdapter(IResource.class));
            }
        } catch (Throwable e) {
            EasyOpenActivator.log("selectionChanged event exception", e);
        }
    }

    protected File getJarFile(IAdaptable adaptable) {
        JarPackageFragmentRoot jpfr = (JarPackageFragmentRoot) adaptable;
        File selected = jpfr.getPath().makeAbsolute().toFile();
        if (!(selected.exists())) {
            File projectFile = new File(jpfr.getJavaProject().getProject().getLocation().toOSString());
            selected = new File(projectFile.getParent() + selected.toString());
        }
        return selected;
    }

}
