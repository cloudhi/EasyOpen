# EasyOpen--支持新版Eclipse快速打开选中文件目录


## EasyOpen项目简易说明

由于easyexplorer等插件长期未更新，在新版的eclipse已经无法使用
抽了点时间开发了个适合新版本的快速打开文件所在文件夹的eclips插件

没什么技术含量，改了个名字叫EasyOpen，怕写的不好，影响了EasyExplorer

本地测试了windows，mac两个平台，是可以正常使用的，linux目前没得环境，理论上也是可以用的。


# 插件安装：

* **简易方式：直接把插件jar包丢到eclipse的dropins目录，然后删掉configuration目录下的org.eclipse.update文件夹，重启eclipse即可**

* **如果想偷懒，懒得自己编译的话，从附件里边下载一个吧**

# 插件效果图：
![输入图片说明](http://git.oschina.net/uploads/images/2015/1120/161022_54688162_113326.png "项目上编辑")
![输入图片说明](http://git.oschina.net/uploads/images/2015/1120/161120_de3aadce_113326.png "文件上编辑")